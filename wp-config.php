<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'git');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', '');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'llT.uvh*T5kg8RsxXEr1R3<M%~BL_[]y/eOsF/`Uoji8Ai`5B=E)|CP7hfVh2Htx');
define('SECURE_AUTH_KEY',  '?P(i!v>d/t2sSTClA!+-.GRH>VS;r[WB876bO0Wp>h=lqgG@Un^en*vic>-LULqv');
define('LOGGED_IN_KEY',    'pQ8^v.5+v,#tw{/4#zH,VZ9JWDDRgx.JKj5OoJpb P2v[r>|~ =*^Yo8Z*(d1Omo');
define('NONCE_KEY',        'F(J6K/z).syU[R.=(7B`:)]kfDa#RO%K,.t1*hNAw==jX+6x] =Zn^Cp!O<QyJ~B');
define('AUTH_SALT',        '(-IX)Jnm8%Hu|Wu[x#V`%hV3<)cC1 DZBEb|H[V0#^+$$Xn={ZS-vR0C[BdQyq=@');
define('SECURE_AUTH_SALT', 'o]Ua) U$[jhN>|WoU|vV@qve~bUR9-/r~Ao~:i)o=AzdCg4~D(8JLB;`[WZEh5hS');
define('LOGGED_IN_SALT',   '8B1J60Nc@LSy(YFl157s}`p&om@OHPYPmf|+4Qk9Ki{@/Ym,<``e}/rAr81;ev6+');
define('NONCE_SALT',       'SDJ`3ZtdG6/F[65AEgNCw$n/sMwA1spQhEA=K<,j&qcGRfq9O!4D7 dDV)St/8/6');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'git_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
